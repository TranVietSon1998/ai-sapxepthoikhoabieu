package ga.main;

import java.util.ArrayList;
import java.util.stream.IntStream;

public class GeneticAlgorithms {
	private Data data;

	public GeneticAlgorithms(Data data) {
		super();
		this.data = data;
	}
	//TÓM TẮT LẠI TOÀN BỘ THUẬT TOÁN CHẠY GA:
		//B1:từ trong quần thể ban đầu,dùng hàm selectournament để chọn ra trong quần thể lớn 2 quần thể con
		//trong 2 quần thể con này dùng hàm fitness để lựa chọn ra 2 cá thể cha tốt nhất để bắt chéo(điều kiện phải vượt qua tỷ lệ bắt chéo)
		//Sau đó dùng hàm crossOverSchedule để bắt chéo để sinh ra cá thể con..Lặp đi lặp lại khi lại đủ quần thể mới thì dừng
	//hàm kết thúc quá trình
		//B2:Từ quần  thể mới phát sinh,nếu vượt quá tỷ lệ đột biến thì thay thế các cá thể cũ bằng cá thể đột biến-->Quần thể mới
		//giải thích chi tiết ở bên dưới
	
	public Population evolve(Population population) {
		return mutatePopulation(crossOverPopulation(population));
	}
	//Hàm bắt chéo  tất cả các cá thể trong quần thể-->trả về 1 quần thể mới.Trong đó 
		//quần thể mới bao gồm những cá thể con đc bắt chéo từ các cá thể cha mẹ

	public Population crossOverPopulation(Population population) {
		Population crossOverPopulation = new Population(population.getSchedules().size(), data);
		//dùng stream để lấy ra cá thể đầu tiên của quần thể
		
		IntStream.range(0, Driver.NUMB_OF_ELITE_SCHEDULES)
				.forEach(x -> crossOverPopulation.getSchedules().set(x, population.getSchedules().get(x)));
		////duyệt từ cá thể đầu tiên đến cá thể cuối cùng trong quần thể
		IntStream.range(Driver.NUMB_OF_ELITE_SCHEDULES, population.getSchedules().size()).forEach(x -> {
			//chỉ thực hiện bắt chéo nếu tỉ lệ bắt chéo lớn hơn tỉ lệ random--tỷ lệ bắt chéo đc xác định bởi người thực hiện
			//Mỗi lần bắt chéo giữa 2 cha mẹ sẽ sinh ra một cả thế con
			if (Driver.CROSSOVER_RATE > Math.random()) {
				
				//Hàm selectTournamentPopulation nhận vào 1 quần thể gồm N cá thể trong đó số lượng cá thể trong quần thể
				//được xác định hàm hằng số "TOURNAMENT_SELECTION_SIZE"
				//Ví dụ hằng số tournament=3..Quần thể A có 10 cá thể,nó sẽ chọn ngẫu nhiên trong quần thể A 3 cá thể rồi sắp
				//xếp theo tứ tự giảm dần độ fitness-->thằng đầu tiên là thằng có fintess cao nhất nó sẽ chọn làm cha mẹ
				//Schedule1 gọi hàm select,nó sẽ lựa chọn ngẫu nhiên 3  cá thể trong quần thể ban đầu,sắp xếp là chọn ra thằng tốt nhất
				//Tương tự Schedule2
				
				Schedule schedule1 = selectTournamentPopulation(population).sortByFitness().getSchedules().get(0);
				Schedule schedule2 = selectTournamentPopulation(population).sortByFitness().getSchedules().get(0);
				crossOverPopulation.getSchedules().set(x, crossOverSchedule(schedule1, schedule2));
			} else
				//nếu không thỏa mãn tỷ lệ bắt chéo thì mặc định đứa đầu tiên sẽ là đứa con
				
				crossOverPopulation.getSchedules().set(x, population.getSchedules().get(x));
		});
		return crossOverPopulation;
	}

	//thực hiện bắt chéo(giao phối giữa 2 cá thể tốt nhất đc gọi là cha và mẹ)-->trả về cá thể
	
	public Schedule crossOverSchedule(Schedule schedule1, Schedule schedule2) {
		//khởi tạo..Ghi nhớ 1 lịch gồm nhiều lớp học  khác nhau
		Schedule crossOverSchedule = new Schedule(data).initialize();
		IntStream.range(0, crossOverSchedule.getClasses().size()).forEach(x -> {
			//đây là quá trình bắt chéo ..nếu vượt qua tỷ lệ 0.5
			//thì nó sẽ lấy của thằng cha
			//ngược lại thì nó sẽ lấy của mẹ...Tùy ý quy định cha mẹ
			
			if (Math.random() > 0.5)
				crossOverSchedule.getClasses().set(x, schedule1.getClasses().get(x));
			else
				crossOverSchedule.getClasses().set(x, schedule2.getClasses().get(x));
		});
		return crossOverSchedule;
	}
	//hàm trả về quẩn thể  đột biến--> trả về quần thể
	
	Population mutatePopulation(Population population) {
		//khơi tạo quần thể đột biến
		Population mutatePopulation= new Population(population.getSchedules().size(), data);
		//lấy ra các cá thể trong quần thể đột biến
		ArrayList<Schedule> schedules=mutatePopulation.getSchedules();
		//lấy ra thằng cá thể đầu tiên
		IntStream.range(0, Driver.NUMB_OF_ELITE_SCHEDULES).forEach(x->schedules.set(x, population.getSchedules().get(x)));
		//chạy từ thằng đầu đến mấy thằng còn lại
		IntStream.range(Driver.NUMB_OF_ELITE_SCHEDULES,population.getSchedules().size()).forEach(x->{
			//Set lại giá trị của cá thể đột biến vào quần thể đột biến
			schedules.set(x,mutateSchedule(population.getSchedules().get(x)));
		});
		return mutatePopulation;

	}
	//Set lại giá trị của cá thể đột biến vào quần thể đột biến
	Schedule mutateSchedule(Schedule mutateSchedule) {
		Schedule schedule = new Schedule(data).initialize();
		IntStream.range(0, mutateSchedule.getClasses().size()).forEach(x->{
			//nếu  tỷ lệ đột biến  lớn hơn giá trị random ngẫu nhiên thì sẽ gán lại giá trị của cá thể đột biến bằng 1 cá thể ngẫu nhiên trong quần thể
			//đây là quá trình phát sinh đột biến
			if(Driver.MUTATION_RATE>Math.random()) mutateSchedule.getClasses().set(x,schedule.getClasses().get(x));
		});
		return mutateSchedule;
	}

	public Population selectTournamentPopulation(Population population) {
		Population tournamentPopulation = new Population(Driver.TOURNAMENT_SELECTION_SIZE, data);
		IntStream.range(0, Driver.TOURNAMENT_SELECTION_SIZE).forEach(x -> {
			tournamentPopulation.getSchedules().set(x,
					population.getSchedules().get((int) (Math.random() * population.getSchedules().size())));
		});
		return tournamentPopulation;
	}
}
