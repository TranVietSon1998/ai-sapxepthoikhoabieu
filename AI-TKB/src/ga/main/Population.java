package ga.main;

import java.util.ArrayList;
import java.util.stream.IntStream;

public class Population {
	private ArrayList<Schedule> schedules;

	public Population(int size, Data data) {
		schedules = new ArrayList<Schedule>(size);
		IntStream.range(0, size).forEach(x -> schedules.add(new Schedule(data).initialize()));
	}

	public ArrayList<Schedule> getSchedules() {
		return schedules;
	}

	public void setSchedules(ArrayList<Schedule> schedules) {
		this.schedules = schedules;
	}
//Dùng chỉ số thích nghi(fitness) để sắp xếp quần thể theo tỷ lệ từ cao đến thấp
	//Hàm trả về quần thể đã được sắp xếp theo thứ tự bởi fitness
	public Population sortByFitness() {
		schedules.sort((schedules1, schedules2) -> {
			int returnValue = 0;
			if (schedules1.getFitness() > schedules2.getFitness())
				returnValue = -1;
			else if (schedules1.getFitness() < schedules2.getFitness())
				returnValue = 1;
			return returnValue;

		});
		return this;
	}

}
