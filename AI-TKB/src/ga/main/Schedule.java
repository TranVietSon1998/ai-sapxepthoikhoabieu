package ga.main;

import java.util.ArrayList;

import ga.data.Class;
import ga.data.Course;
import ga.data.Department;
import ga.data.Instructor;
import ga.data.MeetingTime;
import ga.data.Room;

public class Schedule {
	private ArrayList<Class> classes;
	private boolean isFitnessChanged = true;
	private double fitness = -1;
	private int classNumb = 0;
	private int numberOfConflicts = 0;
	private Data data;

	public Data getData() {
		return data;
	}

	public Schedule(Data data) {
		this.data = data;
		classes = new ArrayList<Class>(data.getNumberOfClasses());
	}

	public Schedule initialize() {
		//kHƠI TẠO CÁC CÁ THỂ DỰA TRÊN CÁC NHIỄM SẮC THỂ LÀ CÁC LỚP...Tổ hợp ngẫu nhiên để tạo thành các cá thể
		new ArrayList<Department>(data.getDepts()).forEach(dept -> {
			dept.getCourses().forEach(course -> {
				Class newClass = new Class(classNumb++, dept, course);
				newClass.setMeetingTime(
						data.getMeetingTimes().get((int) (data.getMeetingTimes().size() * Math.random())));
				newClass.setRoom(data.getRooms().get((int) (data.getRooms().size() * Math.random())));
				newClass.setInstructor(
						course.getInstructor().get((int) (course.getInstructor().size() * Math.random())));
				classes.add(newClass);

			});

		});
		return this;
	}

	public int getNumbOfConflicts() {
		return numberOfConflicts;
	}

	public ArrayList<Class> getClasses() {
		isFitnessChanged = true;
		return classes;
	}

	public double getFitness() {
		if (isFitnessChanged == true) {
			fitness = calculateFitness();
			isFitnessChanged = false;
		}
		return fitness;
	}
//Hàm tính chỉ số thích nghi của cá thể
	//dùng Foreach để duyệt..Bước này xác định các ràng buộc cứng của bài toán 
			//Tính độ Fitness-->keyword về bài toán GA
	private double calculateFitness() {
		numberOfConflicts = 0;
		classes.forEach(x -> {
			if (x.getRoom().getSeatingCapacity() < x.getCourse().getMaxNumberOfStudents())
				numberOfConflicts++;
			classes.stream().filter(y -> classes.indexOf(y) >= classes.indexOf(x)).forEach(y -> {
				if (x.getMeetingTime() == y.getMeetingTime() && x.getId() != y.getId()) {
					if (x.getRoom() == y.getRoom())
						numberOfConflicts++;
					if (x.getInstructor() == y.getInstructor())
						numberOfConflicts++;

				}
			});
		});
		return 1 / (double) (numberOfConflicts + 1);
	}
	////in ra console--quay về lớp toString của Class
	public String toString() {
		String returnValue=new String();
		for(int x=0;x<classes.size()-1;x++) returnValue+=classes.get(x)+",";
		returnValue +=classes.get(classes.size()-1);
		return returnValue;
		
	}

}
