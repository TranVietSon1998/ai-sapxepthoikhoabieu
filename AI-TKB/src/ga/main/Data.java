package ga.main;

import java.io.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import ga.data.Course;
import ga.data.Department;
import ga.data.Instructor;
import ga.data.MeetingTime;
import ga.data.Room;

public class Data {
	private ArrayList<Room> roomList;
	private ArrayList<Instructor> instructorList;
	private ArrayList<Course> courseList;
	private ArrayList<Department> departmentsList;
	private ArrayList<MeetingTime> meetingTimeList;
	private int numberOfClasses = 0;
//Import file dữ liệu từ file exel
	public Data() throws IOException {
		readFileExcel("C:\\Users\\Son\\Desktop\\data.xls");
	}

//	private Data initialize() {
//		Room room1 = new Room("R1", 100);
//		Room room2 = new Room("R2", 120);
//		Room room3 = new Room("R3", 130);
//		Room room4 = new Room("R4", 90);
//		Room room5 = new Room("R5", 100);
//		Room room6 = new Room("R6", 80);
//		Room room7 = new Room("R7", 100);
//		rooms = new ArrayList<Room>(Arrays.asList(room1, room2, room3, room4, room5, room6, room7));
//		MeetingTime meetingTime1 = new MeetingTime("MT1", "MWF 07:00 - 09:30");
//		MeetingTime meetingTime2 = new MeetingTime("MT2", "MWF 09:30 - 11:45");
//		MeetingTime meetingTime3 = new MeetingTime("MT3", "TTH 07:00 - 09:30");
//		MeetingTime meetingTime4 = new MeetingTime("MT4", "TTH 09:30 - 11:45");
//		MeetingTime meetingTime5 = new MeetingTime("MT5", "WFS 12:30 - 14:30");
//		MeetingTime meetingTime6 = new MeetingTime("MT6", "WFS 07:00 - 09:30");
//		MeetingTime meetingTime7 = new MeetingTime("MT7", "MTHF 09:30 - 11:45");
//		MeetingTime meetingTime8 = new MeetingTime("MT8", "MTHF 07:00 - 09:30");
//		MeetingTime meetingTime9 = new MeetingTime("MT9", "TWTH 07:00 - 09:30");
//		MeetingTime meetingTime10 = new MeetingTime("MT10", "TWTH 09:30 - 11:45");
//		meetingTimes = new ArrayList<MeetingTime>(Arrays.asList(meetingTime1, meetingTime2, meetingTime3, meetingTime4,
//				meetingTime5, meetingTime6, meetingTime7, meetingTime8, meetingTime9, meetingTime10));
//		Instructor instructor1 = new Instructor("I1", "MR.BlackNguyen");
//		Instructor instructor2 = new Instructor("I2", "MS.NguyenThiThanhNga");
//		Instructor instructor3 = new Instructor("I3", "MR.PhamVanTinh");
//		Instructor instructor4 = new Instructor("I4", "MS.NguyenThiPhuongTram");
//		Instructor instructor5 = new Instructor("I5", "MR.NguyenPhiHung");
//		Instructor instructor6 = new Instructor("I6", "MS.NguyenThiHuong");
//		instructors = new ArrayList<Instructor>(
//				Arrays.asList(instructor1, instructor2, instructor3, instructor4, instructor5, instructor6));
//		Course course1 = new Course("C1", " LapTrinhCoBan",
//				new ArrayList<Instructor>(Arrays.asList(instructor3, instructor5, instructor2)), 120);
//		Course course2 = new Course("C2", " LapTrinhNangCao",
//				new ArrayList<Instructor>(Arrays.asList(instructor1, instructor2, instructor3, instructor5)), 110);
//		Course course3 = new Course("C3", " CauTrucDuLieuVaGiaiThuat",
//				new ArrayList<Instructor>(Arrays.asList(instructor1, instructor2, instructor6)), 100);
//		Course course4 = new Course("C4", " NhapMonHeDieuHanh",
//				new ArrayList<Instructor>(Arrays.asList(instructor3, instructor4, instructor5)), 130);
//		Course course5 = new Course("C5", " LapTrinhMang",
//				new ArrayList<Instructor>(Arrays.asList(instructor2, instructor4)), 100);
//		Course course6 = new Course("C6", " LyThuyetDoThi",
//				new ArrayList<Instructor>(Arrays.asList(instructor2, instructor4, instructor6, instructor5)), 110);
//		Course course7 = new Course("C7", " DataMining",
//				new ArrayList<Instructor>(Arrays.asList(instructor1, instructor6)), 100);
//		Course course8 = new Course("C8", " AI", new ArrayList<Instructor>(Arrays.asList(instructor1, instructor3)),
//				110);
//		Course course9 = new Course("C9", " DataWareHouse",
//				new ArrayList<Instructor>(Arrays.asList(instructor2, instructor5, instructor4)), 110);
//		Course course10 = new Course("C10", " HeQuanTriCoSoDuLieu",
//				new ArrayList<Instructor>(Arrays.asList(instructor2, instructor6, instructor5)), 100);
//		courses = new ArrayList<Course>(Arrays.asList(course1, course10, course2, course3, course4, course5, course6,
//				course7, course8, course9));
//		Department dept1 = new Department("LapTrinh1", new ArrayList<Course>(Arrays.asList(course1, course2, course3)));
//		Department dept2 = new Department("LapTrinh2", new ArrayList<Course>(Arrays.asList(course4, course5)));
//		Department dept3 = new Department("LapTrinh3", new ArrayList<Course>(Arrays.asList(course6, course7, course8)));
//		Department dept4 = new Department("LapTrinh4", new ArrayList<Course>(Arrays.asList(course9, course10)));
//		depts = new ArrayList<Department>(Arrays.asList(dept1, dept2, dept3, dept4));
//		depts.forEach(x -> numberOfClasses += x.getCourses().size());
//
//		return this;
//	}


	private Object getCellValue(Cell cell) {
		switch (cell.getCellType()) {
		case STRING:
			return cell.getStringCellValue();

		case NUMERIC:
			return cell.getNumericCellValue();

		}
		return null;
	}

	private Data readFileExcel(String fileName) throws IOException {
		try {
			// Đọc một file XSL.
			FileInputStream inputStream = new FileInputStream(new File(fileName));
			// Đối tượng workbook cho file XSL.
			HSSFWorkbook workbook = new HSSFWorkbook(inputStream);

			// Lấy ra sheet Room từ workbook
			HSSFSheet sheet = workbook.getSheetAt(0);
			Iterator<Row> iterator = sheet.iterator();
			roomList = new ArrayList<Room>();
			while (iterator.hasNext()) {
				Row nextRow = iterator.next();
				Iterator<Cell> cellIterator = nextRow.cellIterator();
				Room room = new Room();
				while (cellIterator.hasNext()) {
					Cell nextCell = cellIterator.next();
					int columnIndex = nextCell.getColumnIndex();
					switch (columnIndex) {
					case 0:
						room.setNumber((String) getCellValue(nextCell));
						break;
					case 1:
						room.setSeatingCapacity(new BigDecimal((double) getCellValue(nextCell)).intValue());
						break;
					}
				}
				roomList.add(room);
			}
			// Lấy ra sheet MeetingTime từ workbook
			sheet = workbook.getSheetAt(1);
			iterator = sheet.iterator();
			meetingTimeList = new ArrayList<MeetingTime>();
			while (iterator.hasNext()) {
				Row nextRow = iterator.next();
				Iterator<Cell> cellIterator = nextRow.cellIterator();
				MeetingTime meetingTime = new MeetingTime();
				while (cellIterator.hasNext()) {
					Cell nextCell = cellIterator.next();
					int columnIndex = nextCell.getColumnIndex();
					switch (columnIndex) {
					case 0:
						meetingTime.setId((String) getCellValue(nextCell));
						break;
					case 1:
						meetingTime.setTime((String) getCellValue(nextCell));
						break;
					}
				}
				meetingTimeList.add(meetingTime);
			}
			// Lấy ra sheet Instructor từ workbook
			sheet = workbook.getSheetAt(2);
			iterator = sheet.iterator();
			instructorList = new ArrayList<Instructor>();
			while (iterator.hasNext()) {
				Row nextRow = iterator.next();
				Iterator<Cell> cellIterator = nextRow.cellIterator();
				Instructor instructor = new Instructor();
				while (cellIterator.hasNext()) {
					Cell nextCell = cellIterator.next();
					int columnIndex = nextCell.getColumnIndex();
					switch (columnIndex) {
					case 0:
						instructor.setId((String) getCellValue(nextCell));
						break;
					case 1:
						instructor.setName((String) getCellValue(nextCell));
						break;
					}
				}
				instructorList.add(instructor);
			}
			// Lấy ra sheet Course từ workbook
			sheet = workbook.getSheetAt(3);
			iterator = sheet.iterator();
			courseList = new ArrayList<Course>();
			while (iterator.hasNext()) {
				Row nextRow = iterator.next();
				Iterator<Cell> cellIterator = nextRow.cellIterator();
				Course course = new Course();
				while (cellIterator.hasNext()) {
					Cell nextCell = cellIterator.next();
					int columnIndex = nextCell.getColumnIndex();
					switch (columnIndex) {
					case 0:
						course.setNumber((String) getCellValue(nextCell));
						break;
					case 1:
						course.setName((String) getCellValue(nextCell));
						break;
					case 2:
						course.setMaxNumberOfStudents(new BigDecimal((double) getCellValue(nextCell)).intValue());
						break;
					}
				}
				courseList.add(course);
				if (course.getNumber().equals("C1")) {
					course.addInstructor(instructorList.get(2));
					course.addInstructor(instructorList.get(4));
					course.addInstructor(instructorList.get(1));
				} else if (course.getNumber().equals("C2")) {
					course.addInstructor(instructorList.get(0));
					course.addInstructor(instructorList.get(1));
					course.addInstructor(instructorList.get(2));
					course.addInstructor(instructorList.get(4));
				} else if (course.getNumber().equals("C3")) {
					course.addInstructor(instructorList.get(0));
					course.addInstructor(instructorList.get(1));
					course.addInstructor(instructorList.get(5));
				} else if (course.getNumber().equals("C4")) {
					course.addInstructor(instructorList.get(2));
					course.addInstructor(instructorList.get(3));
					course.addInstructor(instructorList.get(4));

				} else if (course.getNumber().equals("C5")) {
					course.addInstructor(instructorList.get(1));
					course.addInstructor(instructorList.get(3));

				} else if (course.getNumber().equals("C6")) {
					course.addInstructor(instructorList.get(1));
					course.addInstructor(instructorList.get(3));
					course.addInstructor(instructorList.get(5));
					course.addInstructor(instructorList.get(4));
				} else if (course.getNumber().equals("C7")) {
					course.addInstructor(instructorList.get(0));
					course.addInstructor(instructorList.get(5));

				} else if (course.getNumber().equals("C8")) {
					course.addInstructor(instructorList.get(0));
					course.addInstructor(instructorList.get(2));

				} else if (course.getNumber().equals("C9")) {
					course.addInstructor(instructorList.get(1));
					course.addInstructor(instructorList.get(4));
					course.addInstructor(instructorList.get(3));
				} else if (course.getNumber().equals("C10")) {
					course.addInstructor(instructorList.get(1));
					course.addInstructor(instructorList.get(5));
					course.addInstructor(instructorList.get(4));
				}
			}

			// Lấy ra sheet Department từ workbook
			sheet = workbook.getSheetAt(4);
			iterator = sheet.iterator();
			departmentsList = new ArrayList<Department>();
			while (iterator.hasNext()) {
				Row nextRow = iterator.next();
				Iterator<Cell> cellIterator = nextRow.cellIterator();
				Department department = new Department();
				while (cellIterator.hasNext()) {
					Cell nextCell = cellIterator.next();
					int columnIndex = nextCell.getColumnIndex();
					switch (columnIndex) {
					case 0:
						department.setName((String) getCellValue(nextCell));
						break;
					}
				}
				departmentsList.add(department);
				if (department.getName().equals("LapTrinh1")) {
					department.addCourse(courseList.get(0));
					department.addCourse(courseList.get(1));
					department.addCourse(courseList.get(2));
				} else if (department.getName().equals("LapTrinh2")) {
					department.addCourse(courseList.get(3));
					department.addCourse(courseList.get(4));
				} else if (department.getName().equals("LapTrinh3")) {
					department.addCourse(courseList.get(5));
					department.addCourse(courseList.get(6));
					department.addCourse(courseList.get(7));
				} else if (department.getName().equals("LapTrinh4")) {
					department.addCourse(courseList.get(8));
					department.addCourse(courseList.get(9));
				}
				
			}
			departmentsList.forEach(x -> numberOfClasses += x.getCourses().size());

		
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return this;

	}

	
	public ArrayList<Room> getRooms() {
		return roomList;
	}

	public ArrayList<Instructor> getInstructors() {
		return instructorList;
	}

	public ArrayList<Course> getCourses() {
		return courseList;
	}

	public ArrayList<Department> getDepts() {
		return departmentsList;
	}

	public ArrayList<MeetingTime> getMeetingTimes() {
		return meetingTimeList;
	}

	public int getNumberOfClasses() {
		return numberOfClasses;
	}

	public void setNumberOfClasses(int numberOfClasses) {
		this.numberOfClasses = numberOfClasses;
	}

}
