package ga.main;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import ga.data.Class;

public class Driver {
	//khởi tạo các hằng số 
	public static final int POPULATION_SIZE = 10;
	public static final double MUTATION_RATE = 0.2;
	public static final double CROSSOVER_RATE = 0.8;
	public static final int TOURNAMENT_SELECTION_SIZE = 4;
	public static final int NUMB_OF_ELITE_SCHEDULES = 1;
	private int scheduleNumb = 0;
	private int classNumb = 1;
	
	
	static String excelFilePath = "C:\\Users\\Son\\Desktop\\tkb.xls";

	private Data data;
	private static Sheet sheet;
	private static Workbook workbook;

	public static void main(String[] args) throws IOException {
		// dùng thư viện Apache POI 
		workbook = new HSSFWorkbook();
	    sheet = workbook.createSheet("tkb");
	    
	    
		Driver driver = new Driver();
		driver.data = new Data();
		int generationNumber = 0;
		driver.printAvailableData();
		
		//driver.writeTitle(sheet, generationNumber);
		
		System.out.println(" Generation : " + generationNumber);
		System.out.print(" Schedule  |                                                                 ");
		System.out.print(" Classes [dept,class,room,instructor,meeting-time]                                    ");
		System.out.println(
				"                                                                                                      | Fitness |  Conflicts             ");
		System.out.print(
				"-------------------------------------------------------------------------------------------------------------------------------------------------------");
		System.out.println(
				"---------------------------------------------------------------------------------------------------");
		GeneticAlgorithms geneticAlgorithm = new GeneticAlgorithms(driver.data);
		Population population = new Population(Driver.POPULATION_SIZE, driver.data).sortByFitness();
		population.getSchedules()
				.forEach(schedule -> System.out.println("     " + driver.scheduleNumb++ + "  |   " + schedule + " |  "
						+ String.format("%.5f", schedule.getFitness()) + "   |   " + schedule.getNumbOfConflicts()));
		driver.printScheduleAsTable(population.getSchedules().get(0), generationNumber);
		driver.classNumb = 1;
		
		while (population.getSchedules().get(0).getFitness() != 1.0) {
			int z = ++generationNumber; 

			
			System.out.println(" Generation : " + z);
			System.out.print(" Schedule  |                                ");
			System.out.print("Classes  [dept,class,room,instructor,meeting-time]                         "
					+ "                                                 "
					+ "                                             ");
			System.out.println("                                                    |  Fitness  |  Conflicts");
			System.out.print("--------------------------------------------------------------------------------------------"
					+ "-------------------------------------------"
					+ "-----------------------------------------------------------");
			System.out.println("--------------------------------------------------------------");
			population = geneticAlgorithm.evolve(population).sortByFitness();
			driver.scheduleNumb = 0;
			population.getSchedules()
					.forEach(schedule -> System.out.println("   " + driver.scheduleNumb++ + "    |  " + schedule
							+ "   |  " + String.format("%.5f", schedule.getFitness()) + "  |  "
							+ schedule.getNumbOfConflicts()));
			driver.printScheduleAsTable(population.getSchedules().get(0),generationNumber);
			driver.classNumb=1;
			

		}
		// dùng FOS 
		try (FileOutputStream outputStream = new FileOutputStream(excelFilePath)) {
			workbook.write(outputStream);
		}

	}

	private void printScheduleAsTable(Schedule schedule, int generation) throws IOException {
		int z = generation;
		// gọi hàm 
		writeTitle(sheet, z);
		
		ArrayList<Class> classes = schedule.getClasses();
		System.out.print("\n                ");
		System.out.println(
				"     Class #   | Dept       | Course(number , max of students)         |          Room(Capacity)          |            Instructor  ( ID )           |              Meeting Time ( ID )");
		System.out.print("                  ");
		System.out.print("-----------------------------------------------------------------------------------------");
		System.out.println("----------------------------------------");
		//classes.forEach(x -> {
		// chỗ này đổi dòng for each 1 chút
		int rowCount = generation+1;
		for(int i =0; i < classes.size(); i++) {
			Class x = classes.get(i);
			
			int majorIndex = data.getDepts().indexOf(x.getDept());
			int coursesIndex = data.getCourses().indexOf(x.getCourse());
			int roomIndex = data.getRooms().indexOf(x.getRoom());
			int instructorsIndex = data.getInstructors().indexOf(x.getInstructor());
			int meetingTimeIndex = data.getMeetingTimes().indexOf(x.getMeetingTime());
			
			// lấy dữ liệu trong array classes ra string để ghi
			String cClass = String.valueOf(classNumb);
			String depart = data.getDepts().get(majorIndex).getName();
			String course = "" + data.getCourses().get(coursesIndex).getName() + "("+ data.getCourses().get(coursesIndex).getNumber() + ", " + x.getCourse().getMaxNumberOfStudents() +")" ;
			String room = "" + data.getRooms().get(roomIndex).getNumber() + "  (" + x.getRoom().getSeatingCapacity() + ")";
			String instruc ="" + data.getInstructors().get(instructorsIndex).getName() + " ("
					+ data.getInstructors().get(instructorsIndex).getId() + ")";
			String meetingT ="" + data.getMeetingTimes().get(meetingTimeIndex).getTime() + " ("
					+ data.getMeetingTimes().get(meetingTimeIndex).getId() + ")";
			
			System.out.print("                     ");
			System.out.print(String.format(" %1$02d    ", classNumb) + "  | ");
			System.out.print(String.format("%1$4s", data.getDepts().get(majorIndex).getName()) + "   |   ");
			System.out.print(String.format("%1$21s", data.getCourses().get(coursesIndex).getName() + " ("
					+ data.getCourses().get(coursesIndex).getNumber() + ", " + x.getCourse().getMaxNumberOfStudents())
					+ ")          |             ");
			System.out.print(String.format("%1$10s",
					data.getRooms().get(roomIndex).getNumber() + "  (" + x.getRoom().getSeatingCapacity()) + ")            |          ");
			System.out.print(String.format("%1$15s", data.getInstructors().get(instructorsIndex).getName() + " ("
					+ data.getInstructors().get(instructorsIndex).getId() + ")") + "              |           ");
			System.out.println(data.getMeetingTimes().get(meetingTimeIndex).getTime() + " ("
					+ data.getMeetingTimes().get(meetingTimeIndex).getId() + ")");

			classNumb++;
			Row row2 = sheet.createRow(++rowCount);
			writeClass(cClass, depart, course, room, instruc, meetingT, row2 );

		};
		// dùng FOS để export ra file excel
		try (FileOutputStream outputStream = new FileOutputStream(excelFilePath)) {
			workbook.write(outputStream);
		}
		
		if (schedule.getFitness() == 1)
			System.out.println(" Solution found in :" + (generation + 1) + " generations");
		System.out.print("------------------------------------------------------");
		System.out.println("------------------------------------------------------");

	}

	private void printAvailableData() {
		System.out.println("Available Department ==>");
		data.getDepts().forEach(x -> System.out.println(" name: " + x.getName() + " , courses:  " + x.getCourses()));
		System.out.println("\nAvailable Courses ==>");
		data.getCourses().forEach(x -> System.out.println(" course #: " + x.getNumber() + " , name:  " + x.getName()
				+ " ,max # of students:  " + x.getMaxNumberOfStudents() + " , instructor:  " + x.getInstructor()));
		System.out.println("\nAvailable Rooms ==>");
		data.getRooms().forEach(x -> System.out
				.println(" room #:  " + x.getNumber() + " , max seating capacity:  " + x.getSeatingCapacity()));
		System.out.println("\nAvailable Instructors ==>");
		data.getInstructors().forEach(x -> System.out.println(" id: " + x.getId() + " , name: " + x.getName()));
		System.out.println("\nAvailable Meeting Time ==>");
		data.getMeetingTimes()
				.forEach(x -> System.out.println(" id:  " + x.getId() + " x, Meeting Time:  " + x.getTime()));
		System.out.println("==================================================================================");
		System.out.println("==================================================================================");
	}
	// ghi class
	private void writeClass(String cClass, String depart, String course, String room, String instruc, String meetingT, Row row) {
		// ghi dữ liệu cho các dòng trong bảng class
		Cell cell1 = row.createCell(1);
		cell1.setCellValue(cClass);
		
		Cell cell2 = row.createCell(2);
		cell2.setCellValue(depart);
		
		Cell cell3 = row.createCell(3);
		cell3.setCellValue(course);
		
		Cell cell4 = row.createCell(4);
		cell4.setCellValue(room);
		
		Cell cell5 = row.createCell(5);
		cell5.setCellValue(instruc);
		
		Cell cell6 = row.createCell(6);
		cell6.setCellValue(meetingT);
		
	}
	// ghi tiêu đề
	public void writeTitle(Sheet sh, int z) {
		// tạo dòng
		Row row = sh.createRow(z);
		// ghi dữ liệu cho dòng
	    Cell cell = row.createCell(0);
		cell.setCellValue("Generation: " + z);
		
		Cell cell1 = row.createCell(1);
		cell1.setCellValue("Class #");
		
		Cell cell2 = row.createCell(2);
		cell2.setCellValue(" Department ");
		
		Cell cell3 = row.createCell(3);
		cell3.setCellValue("Course(number, max of student)");
		
		Cell cell4 = row.createCell(4);
		cell4.setCellValue("Room(Capacity)");
		
		Cell cell5 = row.createCell(5);
		cell5.setCellValue("Instructor(ID)");
		
		Cell cell6 = row.createCell(6);
		cell6.setCellValue("Meeting Time(ID)");
		
		
		
	}

}
