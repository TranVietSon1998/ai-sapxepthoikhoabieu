package ga.data;

import java.util.ArrayList;

public class Course {
	private String number = null;
	private String name = null;
	private int maxNumberOfStudents;
	private ArrayList<Instructor> instructor;

	public Course(String number, String name , ArrayList<Instructor> instructor,int maxNumberOfStudents) {
		super();
		this.number = number;
		this.name = name;
		this.maxNumberOfStudents = maxNumberOfStudents;
		this.instructor = instructor;
	}
	public Course() {
		instructor = new ArrayList<Instructor>();
	}
	public void addInstructor(Instructor instructor1) {
		instructor.add(instructor1);
	}
	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getMaxNumberOfStudents() {
		return maxNumberOfStudents;
	}

	public void setMaxNumberOfStudents(int maxNumberOfStudents) {
		this.maxNumberOfStudents = maxNumberOfStudents;
	}

	public ArrayList<Instructor> getInstructor() {
		return instructor;
	}

	public void setInstructor(ArrayList<Instructor> instructor) {
		this.instructor = instructor;
	}

	@Override
	public String toString() {
		return this.name;
	}
	

}
