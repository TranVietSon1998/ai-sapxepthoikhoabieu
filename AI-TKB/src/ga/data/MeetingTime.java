package ga.data;

public class MeetingTime {
	private String id;
	private String time;

	public MeetingTime(String id, String time) {
		super();
		this.id = id;
		this.time = time;
	}
	public MeetingTime() {
		// TODO Auto-generated constructor stub
	}
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

}
