package ga.data;

public class Room {
	private String number;
	private int seatingCapacity;

	public Room(String number, int seatingCapacity) {
		super();
		this.number = number;
		this.seatingCapacity = seatingCapacity;
	}
	public Room() {
		
	}
	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public int getSeatingCapacity() {
		return seatingCapacity;
	}

	public void setSeatingCapacity(int seatingCapacity) {
		this.seatingCapacity = seatingCapacity;
	}

}
