package ga.data;

public class Instructor {
	private String id;
	private String name;

	public Instructor(String id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	public Instructor() {
		// TODO Auto-generated constructor stub
	}
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}
	

}
